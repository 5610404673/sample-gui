package sample;

public class Control {
	BankAccount account;
	
	public Control(){
		account = new BankAccount();	
	}
	
	public Control(double initialbalance){
		account = new BankAccount(initialbalance);	
	}
	
	public void addmoney(double amount){
		account.deposit(amount);
		
	}
	
	public void deletemoney(double amount){
		account.withdraw(amount);
	}
	
	public double showmoney(){
		return account.getBalance();
	}
}
